<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Rest_api extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }
    function banner_get() {
        $id = $this->get('id');
        if ($id == '') {
            $banner = $this->db->get('banner')->result();
        } else {
            $this->db->where('id', $id);
            $banner = $this->db->get('banner')->result();
        }
		
        $this->response($banner, REST_Controller::HTTP_OK);
    }
	
	function kategori_get() {
        $id = $this->get('id');
        
        if ($id == '') {
            $kategori = $this->db->get('kategori')->result();
        } else {
            $this->db->where('id_kategori', $id);
            $kategori = $this->db->get('kategori')->result();
        }
		
        $this->response($kategori, REST_Controller::HTTP_OK);
    }
	function filter_get() {
        $id = $this->get('id');
        
        if ($id == '') {
            $filter = $this->db->get('filter')->result();
        } else {
            $this->db->where('id_filter', $id);
            $filter = $this->db->get('filter')->result();
        }
		
        $this->response($filter, REST_Controller::HTTP_OK);
    }
	
	function produk_get() {
        $id = $this->get('id');
		$search = $this->get('search');
		$kategori = $this->get('kategori');
		$filter = explode(",",$this->get('filter'));
		$page= $this->get('page');
		if($page == '')
			$page=1;
		$size = 5;
        $page = $size*($page-1);
        if ($id == '') {
			if ($kategori != '') 
				$this->db->where('id_kategori', $kategori);
			if ($this->get('filter') != '') 
				$this->db->where_in('id_filter', $filter);
			$this->db->like('nama', $search);
			$this->db->order_by('RAND()');
			$this->db->group_by('produk.id_produk'); 
			$this->db->join('filter_produk', 'produk.id_produk = filter_produk.id_produk');
            $produk = $this->db->get('produk',$size,$page)->result();
        } else {
            $this->db->where('id_produk', $id);
            $produk = $this->db->get('produk',$size,$page)->result();
        }
		
        $this->response($produk, REST_Controller::HTTP_OK);
    }


 }
?>