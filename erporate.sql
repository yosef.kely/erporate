-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 25, 2019 at 07:57 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `erporate`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE IF NOT EXISTS `banner` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `title`, `foto`) VALUES
(1, 'tes', 'tes'),
(2, 'no 2', 'no 2');

-- --------------------------------------------------------

--
-- Table structure for table `filter`
--

CREATE TABLE IF NOT EXISTS `filter` (
`id_filter` int(11) NOT NULL,
  `tipe_filter` varchar(50) NOT NULL,
  `nama_filter` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `filter`
--

INSERT INTO `filter` (`id_filter`, `tipe_filter`, `nama_filter`) VALUES
(1, 'Populer', 'Chinese'),
(2, 'Populer', 'Halal'),
(3, 'Populer', 'Indonesian'),
(4, 'Populer', 'Italian'),
(5, 'Populer', 'Japanese'),
(6, 'Populer', 'Non Halal'),
(7, 'Merchant', 'Premium'),
(8, 'Reguler', 'American'),
(9, 'Reguler', 'Asian'),
(10, 'Reguler', 'Bakery'),
(11, 'Reguler', 'Coffe'),
(12, 'Reguler', 'Dessert'),
(13, 'Reguler', 'Khas Bali'),
(14, 'Populer', 'Kue Artis'),
(15, 'Populer', 'Sambal');

-- --------------------------------------------------------

--
-- Table structure for table `filter_produk`
--

CREATE TABLE IF NOT EXISTS `filter_produk` (
`id_filter_produk` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `id_filter` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `filter_produk`
--

INSERT INTO `filter_produk` (`id_filter_produk`, `id_produk`, `id_filter`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 3),
(4, 3, 1),
(5, 4, 5),
(6, 6, 5),
(7, 7, 7),
(8, 7, 1),
(9, 8, 2),
(10, 9, 10);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
`id_kategori` int(11) NOT NULL,
  `kategori` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `kategori`) VALUES
(1, 'Under 30K'),
(2, 'Halal'),
(3, 'Promo'),
(4, 'Kue Artis');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE IF NOT EXISTS `produk` (
`id_produk` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `id_kategori`, `nama`, `deskripsi`, `harga`) VALUES
(1, 1, 'Opak Balado', 'opak singkong', 12000),
(2, 1, 'Keris - keripik crispy', 'keris enak', 15000),
(3, 2, 'Kebab mini keju', 'kebab enak', 35000),
(4, 2, 'Rendang paru takana', 'Rendang paru takana', 90000),
(5, 2, 'Luwak kopi', 'Luwak kopi enak banget', 240000),
(6, 3, 'Kapri kecil', 'Kapri kecil enak loh', 54000),
(7, 3, 'piscok cakra', 'enak banget', 40000),
(8, 3, 'makaroni', 'ini juga enak', 8520),
(9, 3, 'sagu keju', 'sagu karya bangsawan enak banget', 750000),
(10, 4, 'pia uluwatu', 'pia buatan artis bali', 50000),
(11, 4, 'klembeen tapai', 'ini kue artis loh', 30000),
(12, 2, 'affinois', 'ini lapis legit', 255000),
(13, 2, 'ceker', 'ini krupuk ceker ayam', 36800),
(14, 1, 'hanjuang', 'pokoknya enak', 17000),
(15, 4, 'jenkol', 'jengkol balado super', 54000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `filter`
--
ALTER TABLE `filter`
 ADD PRIMARY KEY (`id_filter`);

--
-- Indexes for table `filter_produk`
--
ALTER TABLE `filter_produk`
 ADD PRIMARY KEY (`id_filter_produk`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
 ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
 ADD PRIMARY KEY (`id_produk`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `filter`
--
ALTER TABLE `filter`
MODIFY `id_filter` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `filter_produk`
--
ALTER TABLE `filter_produk`
MODIFY `id_filter_produk` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
